import Tile from "./class.tile";
import Wall from "./class.wall";

const Maze = class {

    // Properties
    tiles:object[] = [];

    constructor(public x: number, public y: number) {
        this.tiles.length = x * y;
        
        // Populate empty tiles
        for (let t = 0; t < x * y; t++) {
            if (this.isWall(t)) {
                this.tiles[t] = new Wall();
            }
            else {
                this.tiles[t] = new Tile();
            }
        }
    }

    isWall(tile: number) {
        return (
            tile < this.x  || // Bottom walls
            tile >= (this.x * (this.y - 1)) || // Top walls
            tile % this.x == 0 || // Left walls
            (tile + 1) % this.x == 0 // Right walls
        )
    }

    printable() {
        return this.tiles.map((tile, index) => {
            // Walls
            if (tile instanceof Wall) {
                // Right-edged Walls
                if ((index + 1) % this.x == 0) {
                    return '\u25A0' + '\n';
                }
                // Other Walls
                return '\u25A0' + ' ';
            }
            // Non-Walls
            return '  ';
        }).join('')
    }

}

export default Maze;